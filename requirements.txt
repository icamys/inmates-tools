beautifulsoup4==4.6.0
bs4==0.0.1
certifi==2018.4.16
chardet==3.0.4
CouchDB==1.2
idna==2.7
numpy==1.14.5
python-dotenv==0.8.2
requests==2.19.1
urllib3==1.23
