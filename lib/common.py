import errno
import os


def create_dir_structure(parent_dir):
    if parent_dir is None:
        return 1

    if os.path.exists(parent_dir) is False:
        return 2

    for i in range(0, 16):
        level_1_dir = parent_dir + '/' + str(format(i, 'x'))
        try:
            os.makedirs(level_1_dir)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

        for j in range(0, 16):
            for k in range(0, 16):
                level_2_dir = level_1_dir + '/' + str(format(j, 'x')) + str(format(k, 'x'))
                try:
                    os.makedirs(level_2_dir)
                except OSError as e:
                    if e.errno != errno.EEXIST:
                        raise