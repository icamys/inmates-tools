import logging
import re

import bs4
import couchdb
import requests
from bs4 import BeautifulSoup

from lib.parser.BaseParser import BaseParser
from lib.service import NameEncoder


class Parser(BaseParser):

    def __init__(self, name_encoder: NameEncoder, img_dir: str):
        BaseParser.__init__(self, name_encoder, img_dir)
        self.url = 'http://www.doc.state.al.us'

    def parse(self, record: couchdb.Document):

        soup = BeautifulSoup(record.get('html'), 'html.parser')

        doc = {}

        name_tag = soup.find('span', id='MainContent_DetailsView2_Label1')
        if name_tag is not None:
            full_name_str: str = name_tag.text
            full_name_lst = full_name_str.split(', ')

            if full_name_lst[0]:
                doc['last_name'] = full_name_lst[0].strip()
                doc['last_name_code'] = self.name_encoder.encode(
                    doc['last_name'])  # todo: make async call

            if full_name_lst[1]:
                doc['first_name'] = full_name_lst[1].strip()
                doc['first_name_code'] = self.name_encoder.encode(
                    doc['first_name'])  # todo: make async call

        ais = soup.find('span', id='MainContent_DetailsView2_Label2')
        if ais is not None:
            doc['ais'] = ais.text.strip()

        institution = soup.find('span', id='MainContent_DetailsView2_Label3')
        if institution is not None:
            doc['institution'] = institution.text.strip()

        details_table = soup.find('table', id='MainContent_DetailsView1')
        if details_table is not None:
            table_values = list()
            for i, col in enumerate(details_table.find_all('td')):
                col_str: str = col.text \
                    .replace('\\r', '') \
                    .replace('\\n', '') \
                    .replace('\n', '') \
                    .strip(' :')
                table_values.append(col_str)

            for i, value in enumerate(table_values):
                if i % 2 == 1:
                    continue
                doc[value.lower().replace(' ', '_')] = table_values[i + 1].lower()

        aliases_tag = soup.find('span', id='MainContent_lvAlias_AliasLabel0_0')
        if aliases_tag is not None:
            doc['aliases'] = aliases_tag.text.strip()
            pass

        if 'Aliases:' in record.get('html'):
            aliases = list()
            alias_tag_index = 0
            alias_tag_id_prefix = 'MainContent_lvAlias_AliasLabel0_'
            alias_tag_id = alias_tag_id_prefix + str(alias_tag_index)
            alias_tag = soup.find('span', id=alias_tag_id)

            while alias_tag is not None:
                alias_tag_index += 1
                aliases.append(alias_tag.text.strip())
                alias_tag_id = alias_tag_id_prefix + str(alias_tag_index)
                alias_tag = soup.find('span', id=alias_tag_id)

            if len(aliases) > 0:
                doc['aliases'] = aliases

        if 'Scars, Marks and Tattoos' in record.get('html'):
            scars = list()
            scar_tag_index = 0
            scar_tag_id_prefix = 'MainContent_lvScars_descriptLabel_'
            scar_tag_id = scar_tag_id_prefix + str(scar_tag_index)
            scar_tag = soup.find('span', id=scar_tag_id)

            while scar_tag is not None:
                scar_tag_index += 1
                scars.append(scar_tag.text.strip())
                scar_tag_id = scar_tag_id_prefix + str(scar_tag_index)
                scar_tag = soup.find('span', id=scar_tag_id)

            if len(scars) > 0:
                doc['scars_marks_tattoos'] = scars

        incarcerations_tbl = soup.find('table', id='MainContent_gvSentence')
        if incarcerations_tbl is not None:
            terms = list()

            for i, row in enumerate(incarcerations_tbl.find_all('tr')):

                if row.attrs.get('align') == 'left' and row.attrs.get('bgcolor') == '#2255AA':
                    pass # Term table header
                elif row.attrs.get('align') == 'left':
                    columns = row.find_all('td')

                    if len(columns) > 10:
                        term = {}

                        m = re.search(r"(\d+)/(\d+)/(\d+)", columns[1].text.strip())

                        if m is not None:
                            term['admit_date'] = {
                                'd': int(m.group(1)),
                                'm': int(m.group(2)),
                                'y': int(m.group(3)),
                            }

                        m = re.search(r"(\d+)Y (\d+)M (\d+)D", columns[2].text.strip())

                        if m is not None:
                            term['total_term'] = {
                                'd': int(m.group(3)),
                                'm': int(m.group(2)),
                                'y': int(m.group(1)),
                            }

                        m = re.search(r"(\d+)Y (\d+)M (\d+)D", columns[3].text.strip())

                        if m is not None:
                            term['time_served'] = {
                                'd': int(m.group(3)),
                                'm': int(m.group(2)),
                                'y': int(m.group(1)),
                            }

                        m = re.search(r"(\d+)Y (\d+)M (\d+)D",
                                      columns[5].text.strip())

                        if m is not None:
                            term['good_time_received'] = {
                                'd': int(m.group(3)),
                                'm': int(m.group(2)),
                                'y': int(m.group(1)),
                            }

                        term['parole_status'] = columns[9].text.strip()

                        terms.append(term)
                else:
                    sentences_tbl = row.find('table', id=lambda x: x and x.startswith('MainContent_gvSentence_GridView'))
                    if sentences_tbl is not None:
                        sentences = list()
                        col_fmt = sentences_tbl.attrs.get('id') + '_Label{col}_{row}'

                        for i, sts_row in enumerate(sentences_tbl.find_all('tr')):
                            if i == 0:  # skip table header
                                continue

                            sentence = dict(
                                case_no=soup.find('span', id=col_fmt.format(col=1, row=i - 1)).text.strip(),
                                sentenced=soup.find('span', id=col_fmt.format(col=2, row=i - 1)).text.strip(),
                                offence=soup.find('span', id=col_fmt.format(col=3, row=i - 1)).text.strip(),
                                term=soup.find('span', id=col_fmt.format(col=4, row=i - 1)).text.strip(),
                                sentence_type=soup.find('span', id=col_fmt.format(col=5, row=i - 1)).text.strip(),
                                commit_county=soup.find('span', id=col_fmt.format(col=6, row=i - 1)).text.strip(),
                            )

                            m = re.search(r"(\d+)/(\d+)/(\d+)", sentence.get('sentenced'))

                            if m is not None:
                                sentence['sentenced'] = {
                                    'y': m.group(3),
                                    'm': m.group(1),
                                    'd': m.group(2),
                                }

                            m = re.search(r"(\d+)Y (\d+)M (\d+)D", sentence.get('term'))

                            if m is not None:
                                sentence['term'] = {
                                    'y': m.group(1),
                                    'm': m.group(2),
                                    'd': m.group(3),
                                }

                            sentences.append(sentence)

                        terms[len(terms) - 1]['sentences'] = sentences

            if len(terms) > 0:
                doc['terms'] = terms

        img: bs4.element.Tag = soup.find('img', id='MainContent_imgInmate')
        if img is not None:
            src: str = img.attrs.get('src')

            if src != 'photos%0aotfound.jpg':
                src = self.url + '/' + src.replace('\\', '/')

                if self.is_downloadable_image(src):
                    current_request = 1

                    while current_request <= self.request_loop_guard:
                        try:
                            r: requests.Response = requests.get(
                                url=src,
                                allow_redirects=True,
                                timeout=self.request_timeout
                            )
                            break
                        except Exception:
                            current_request += 1

                    if current_request <= self.request_loop_guard:
                        self.hashing_obj.update(r.content)
                        img_hash: str = self.hashing_obj.hexdigest()
                        img_file_path = self.get_img_path_by_hash(img_hash) + '/' + img_hash + '.jpg'
                        open(self.img_root_dir + '/' + img_file_path, 'wb').write(r.content)
                        doc['photo'] = img_file_path

        return self.doc_clean(doc)

    @staticmethod
    def doc_clean(doc: dict):
        if doc.get('institution') == 'UNASSIGNED':
            del doc['institution']

        if doc.get('custody') == 'none':
            del doc['custody']

        if doc.get('scars_marks_tattoos') is not None \
                and doc.get('scars_marks_tattoos')[0] == 'No known scars, marks, or tattoos':
            del doc['scars_marks_tattoos']

        return doc
