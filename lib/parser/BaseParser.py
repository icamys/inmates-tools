import hashlib
import requests

from lib.service import NameEncoder


class BaseParser:
    def __init__(self, name_encoder: NameEncoder, img_dir: str):
        self.name_encoder = name_encoder
        self.img_root_dir = img_dir
        self.hashing_obj: hashlib.md5 = hashlib.new('md5')
        self.request_timeout = 2
        self.request_loop_guard = 5

    @staticmethod
    def is_downloadable_image(url):
        """
        Does the url contain a downloadable resource
        """
        h = requests.head(url, allow_redirects=True)
        header = h.headers
        content_type = header.get('content-type', '').lower()
        content_length = int(header.get('content-length', 0))
        return 'image' in content_type and (0 < content_length < 1024 * 1024)

    @staticmethod
    def get_img_path_by_hash(hash: str):
        return hash[-1:] + '/' + hash[-3:-1]
