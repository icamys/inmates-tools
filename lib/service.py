import numpy
import requests


class NameEncoder(dict):

    def __init__(self, config: dict):
        super(self.__class__, self).__init__(config)
        self.url_format = 'http://{}:{}?name={{}}'.format(config.get('host'), config.get('port'))

    def encode(self, name: str):
        response: requests.Response = requests.get(self.url_format.format(name))
        decoded_content = response.content.decode("utf-8")

        if decoded_content is None or decoded_content == '':
            return None

        return int(numpy.uint64(decoded_content))
