import couchdb
import requests
import logging
from requests.adapters import HTTPAdapter
from urllib3 import Retry


class BaseScraper:

    def __init__(self, db: couchdb.Database, logger: logging.Logger):
        self.headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
            'Cache-Control': 'max-age=0',
            'Origin': 'http://www.doc.state.al.us',
            'Host': 'www.doc.state.al.us',
        }
        self.db = db
        self.logger = logger
        self.session = self.requests_retry_session()
        self.request_timeout = 2

    @staticmethod
    def requests_retry_session(
        retries=3,
        backoff_factor=0.3,
        status_forcelist=(500, 502, 504),
        session=None,
    ):
        session = session or requests.Session()
        retry = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=backoff_factor,
            status_forcelist=status_forcelist,
        )
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session
