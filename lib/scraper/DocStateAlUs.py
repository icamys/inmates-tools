import logging
import re
import time

import couchdb
import requests
from string import ascii_lowercase
from bs4 import BeautifulSoup
from lib.scraper.BaseScraper import BaseScraper


class Scraper(BaseScraper):

    def __init__(self, db, logger: logging.Logger):
        BaseScraper.__init__(self, db, logger)

        self.search_page_url = 'http://www.doc.state.al.us/InmateSearch'
        self.inmate_info_url = 'http://www.doc.state.al.us/InmateInfo'

        self.search_page_hidden_input_names = ['__LASTFOCUS', '__EVENTTARGET', '__EVENTARGUMENT',
                                               '__VIEWSTATE',
                                               '__VIEWSTATEGENERATOR', '__EVENTVALIDATION']

        self.search_results_page_input_values = ['__EVENTTARGET', '__EVENTARGUMENT', '__VIEWSTATE',
                                                 '__VIEWSTATEGENERATOR', '__VIEWSTATEENCRYPTED',
                                                 '__EVENTVALIDATION']

        self.search_form_post_data = {
            'ctl00$MainContent$txtAIS': '',
            'ctl00$MainContent$txtFName': '',
            'ctl00$MainContent$txtLName': '',
            'ctl00$MainContent$btnSearch': 'Search'
        }

    @staticmethod
    def collect_hidden_input_values_from_soup(input_names, soup):
        values = {}

        for name in input_names:
            tag = soup.find(name='input', attrs={'name': name})

            if tag is not None:
                values[name] = tag.attrs['value']
            else:
                values[name] = ''

        return values

    def scrape(self):
        self.logger.info('Stage 1: Getting search page input fields')

        r: requests.Response = self.session.get(self.search_page_url, timeout=self.request_timeout)

        if r.status_code >= 400:
            self.logger.error('Unexpected status code from server received')
            exit(1)

        if 'Server Error' in r.content.decode("utf-8"):
            self.logger.error('Server error')
            self.logger.error('URL: {}'.format(self.inmate_info_url))
            open('./scraper-error-{}.html'.format(time.time()), 'wb').write(r.content)
            exit(1)

        soup = BeautifulSoup(r.content, 'html.parser')

        search_form_hidden_input_values = \
            self.collect_hidden_input_values_from_soup(self.search_page_hidden_input_names, soup)

        self.logger.info('Stage 2: Iterating over all last names')
        for char_first in ascii_lowercase:
            for char_second in ascii_lowercase:
                search_query = char_first + char_second

                post_data = {**self.search_form_post_data, **search_form_hidden_input_values}
                post_data.update({'ctl00$MainContent$txtLName': search_query})

                self.logger.info('Querying "%s"', search_query)
                r: requests.Response = self.session.post(
                    self.search_page_url,
                    data=post_data,
                    timeout=self.request_timeout,
                )

                soup = BeautifulSoup(r.content, 'html.parser')
                self.__scrape_list(soup)

    def __scrape_list(self, soup: BeautifulSoup):
        current_page = 1
        total_pages = 1

        while current_page <= total_pages:
            inmate_tbl = soup.find('table', id="MainContent_gvInmateResults")

            if inmate_tbl is None:
                break

            inmate_tbl_rows = inmate_tbl.find_all('tr')
            inmate_tbl_rows = inmate_tbl_rows[1:]  # removing table heading row

            total_pages_tag = \
                soup.find(name='span', attrs={'id': 'MainContent_gvInmateResults_lblPages'})

            if total_pages_tag is not None:
                total_pages = int(total_pages_tag.text)
                inmate_tbl_rows = inmate_tbl_rows[:-1]  # removing table pagination row

            inmate_serial_numbers = []

            for row in inmate_tbl_rows:
                inmate_serial_numbers.append(row.select('td:nth-of-type(1)')[0].text)

            current_page_tag = \
                soup.find(name='span',
                          attrs={'id': 'MainContent_gvInmateResults_lblCurrent'})

            if current_page_tag is not None:
                current_page = int(current_page_tag.text)

            self.logger.info('Page %d of %d', current_page, total_pages)

            inmate_links = soup.find_all("a", id=re.compile(
                "^MainContent_gvInmateResults_lnkInmateName_"))

            assert len(inmate_serial_numbers) == len(inmate_links)

            self.logger.info('Found inmate profiles: %d', len(inmate_links))

            page_hidden_input_values = self.collect_hidden_input_values_from_soup(
                self.search_results_page_input_values, soup
            )

            self.__scrape_profile(inmate_serial_numbers, inmate_links, page_hidden_input_values)

            if current_page < total_pages:
                next_page_tag = \
                    soup.find(name='input',
                              attrs={'id': 'MainContent_gvInmateResults_btnNext'})

                if next_page_tag is not None:
                    self.logger.info('Next page tag found')
                    post_data = page_hidden_input_values.copy()

                    # X and Y coordinates of the click on next button image
                    post_data['ctl00$MainContent$gvInmateResults$ctl28$btnNext.x'] = 10
                    post_data['ctl00$MainContent$gvInmateResults$ctl28$btnNext.y'] = 10

                    current_headers = self.headers.copy()
                    current_headers['Referer'] = self.inmate_info_url

                    self.logger.info('Requesting next page')
                    r: requests.Response = self.session.post(
                        self.inmate_info_url,
                        data=post_data,
                        headers=current_headers,
                        timeout=self.request_timeout,
                    )
                    self.logger.info('Response received')

                    if 'Server Error' in r.content.decode("utf-8"):
                        self.logger.error('Response contains server error message')
                        self.logger.error('URL: {}'.format(self.inmate_info_url))
                        self.logger.error('Data sent: {}'.format(post_data))
                        open('./scraper-error-{}.html'.format(time.time()), 'wb').write(r.content)
                        exit(1)
                    else:
                        self.logger.info('Response is valid')

                    soup = BeautifulSoup(r.content, 'html.parser')

            current_page += 1

    def __scrape_profile(self, serial_numbers, links, input_values):
        html_pages = []

        for i in range(len(links)):
            link = links[i]
            serial_number = serial_numbers[i]

            m = re.search(r"javascript:__doPostBack\('(.+)',''\)", link.attrs.get('href'))
            post_data = input_values.copy()
            post_data['__EVENTTARGET'] = m.group(1)

            try:
                self.logger.info('Requesting for profile...')
                r: requests.Response = requests.post(
                    url=self.inmate_info_url,
                    data=post_data,
                    timeout=self.request_timeout
                )
            except requests.Timeout:
                self.logger.warn('Request timed out. Skipping this profile')
                continue
            else:
                self.logger.info('Response received')

            html = r.content.decode("utf-8")

            if 'Server Error' in html:
                self.logger.error('Response contains server error message')
                self.logger.error('URL: {}'.format(self.inmate_info_url))
                self.logger.error('Data sent: {}'.format(post_data))
                open('./scraper-error-{}.html'.format(time.time()), 'wb').write(r.content)
                exit(1)
            else:
                self.logger.info('Response is valid')

            self.logger.info('Checking html source for %s', serial_number)

            existing_doc = None
            try:
                existing_doc: couchdb.Document = self.db[serial_number]
                self.logger.info('Found page with this id in database')
            except couchdb.http.ResourceNotFound:
                pass  # not found is ok

            if existing_doc is None:
                self.logger.info('Found new profile. Enqueue page for saving...')
                html_pages.append({
                    '_id': serial_number,
                    'html': html,
                })
                self.logger.info('Done')
            else:
                existing_soup = BeautifulSoup(existing_doc.get('html'), 'html.parser')
                existing_article = existing_soup.find(name='div', attrs={'id': 'content'}).text

                remote_soup = BeautifulSoup(html, 'html.parser')
                remote_article = remote_soup.find(name='div', attrs={'id': 'content'}).text

                if existing_article != remote_article:
                    self.logger.info('Found difference. Enqueue for save...')

                    html_pages.append({
                        '_id': serial_number,
                        '_rev': existing_doc.get('_rev'),
                        'html': html,
                    })
                else:
                    self.logger.info('No diff. Skipping...')

        if len(html_pages) > 0:
            self.logger.info('Saving pages to database...')
            result = self.db.update(html_pages)

            if result[0][0] is False:
                self.logger.error('Error: {}. _id = {}'.format(result[0][2], result[0][1]))
                exit(1)

            self.logger.info('Done')
        else:
            self.logger.info('No new info')
