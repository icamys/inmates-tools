import os
import couchdb


def create_db_conn(db_host, db_name, db_port, db_user, db_password):
    """
    :return: a `Database` object representing the created database
    :rtype: `Database`
    """
    couch_server = couchdb.Server(
        "http://%s:%s@%s:%s/" % (db_user, db_password, db_host, db_port)
    )

    if db_name in couch_server:
        input_db: couchdb.Database = couch_server[db_name]
    else:
        input_db: couchdb.Database = couch_server.create(db_name)

    return input_db
