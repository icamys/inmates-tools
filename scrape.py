import logging
import os
import time

from dotenv import load_dotenv
from pathlib import Path
from lib.db import create_db_conn
from lib.scraper.DocStateAlUs import Scraper

if __name__ == '__main__':
    env_path = Path('.') / '.env'
    load_dotenv(dotenv_path=env_path)

    output_db = create_db_conn(
        db_name='raw__doc_state_al_us',
        db_host=os.getenv("COUCHDB_HOST"),
        db_port=os.getenv("COUCHDB_PORT"),
        db_user=os.getenv("COUCHDB_USER"),
        db_password=os.getenv("COUCHDB_PASSWORD"),
    )

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger('scraper')

    scraper = Scraper(output_db, logger)

    time_start = time.time()
    scraper.scrape()
    time_end = time.time()

    logger.info('Scraping done. Time elapsed: %ds', (time_end - time_start))
