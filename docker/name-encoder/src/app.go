package main

import (
	"net/http"
	"hash/crc64"
	"strconv"
	"flag"
	"fmt"
)

var (
	// @see https://en.wikipedia.org/wiki/Cyclic_redundancy_check
	crc64Table = crc64.MakeTable(0xC96C5795D7870F42)
)

func encode(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()

	name := params.Get("name")

	nameBytes := []byte(name)

	w.Write([]byte(strconv.FormatUint(crc64.Checksum(nameBytes, crc64Table), 10)))
}

func faviconHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(204)
}

func main() {
	port := flag.String("p", "9559", "Specify the running server port")

	flag.Parse()

	http.HandleFunc("/", encode)
	http.HandleFunc("/favicon.ico", faviconHandler)

	fmt.Printf("Listening on 127.0.0.1:%s\n", *port)
	fmt.Printf("Example request: http://127.0.0.1:%s?name=Ronald\n", *port)

	if err := http.ListenAndServe(":" + *port, nil); err != nil {
		panic(err)
	}
}
