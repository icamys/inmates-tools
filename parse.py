import logging
import os

import couchdb
from dotenv import load_dotenv
from pathlib import Path
from lib.common import create_dir_structure
from lib.parser.DocStateAlUs import Parser
from lib.db import create_db_conn
from lib.service import NameEncoder

if __name__ == '__main__':
    env_path = Path('.') / '.env'
    load_dotenv(dotenv_path=env_path)

    create_dir_structure(parent_dir=os.getenv("IMG_ROOT_DIR"))

    input_db = create_db_conn(
        db_name='raw__doc_state_al_us',
        db_host=os.getenv("COUCHDB_HOST"),
        db_port=os.getenv("COUCHDB_PORT"),
        db_user=os.getenv("COUCHDB_USER"),
        db_password=os.getenv("COUCHDB_PASSWORD"),
    )

    output_db: couchdb.Database = create_db_conn(
        db_name='json__doc_state_al_us',
        db_host=os.getenv("COUCHDB_HOST"),
        db_port=os.getenv("COUCHDB_PORT"),
        db_user=os.getenv("COUCHDB_USER"),
        db_password=os.getenv("COUCHDB_PASSWORD"),
    )

    name_encoder = NameEncoder({
        'host': os.getenv("NAME_ENCODER_HOST"),
        'port': os.getenv("NAME_ENCODER_PORT"),
    })

    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger('parser')

    parser = Parser(name_encoder, os.getenv("IMG_ROOT_DIR"))

    limit = 20
    skip = 0
    res = True

    while res is not None:
        records = input_db.find({'selector': {}, 'limit': limit, 'skip': skip})

        for record in records:
            doc = parser.parse(record)

            existing_docs_iter: map = output_db.find({'selector': {
                'last_name_code': doc.get('last_name_code'),
                'first_name_code': doc.get('first_name_code'),
            }})

            existing_docs = list()

            for d in existing_docs_iter:
                existing_docs.append(d)

            if len(existing_docs) == 0:
                logger.info('Creating doc')
                output_db.save(doc)
            elif len(existing_docs) == 1:
                logger.info('Updating doc {}'.format(existing_docs[0]['_id']))
                doc['_rev'] = existing_docs[0]['_rev']
                output_db[existing_docs[0]['_id']] = doc
            else:
                logger.warning(
                    'Found duplicate with first name "{}" and last name {}'.format(doc['first_name'], doc['last_name']))

        skip += limit
