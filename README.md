## Usage ##

1. Clone the repo and change working directory:

        git clone git@bitbucket.org:icamys/inmates-info.git \
        && cd inmates-info

1. Install docker and docker-compose:

        ./install_docker_compose.sh

1. Install python3-pip and python3-venv

        apt-get install -y python3-pip python3-venv

1. Create and activate environment

        python3.6 -m venv venv && source ./venv/bin/activate

1. Install requirements

        pip3 install -r requirements.txt

1. Prepare environment:

        cp sample.env .env
        vim .env

1. Start dockerized name-encoder:

        docker-compose up -d name-encoder

1. Start scraping

        python3.6 scrape.py